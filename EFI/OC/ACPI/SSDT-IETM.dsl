// TERRA PC MICRO NUC Ausaetzliches Gereat Thermal subsystem

DefinitionBlock("", "SSDT", 2, "hack", "UX330UAK", 0x00000000)
{
    External(\_SB.PCI0, DeviceObj) // Ausaetzliches Gereat Thermal subsystem


        Scope(\_SB.PCI0)
        {

            Device(IETM) // Zusaetzliches Gereat Thermal subsystem
            {
                Name(_ADR, 0x00140002) // _ADR: Address
                Name(IETM, One) 
                Method(_DSM, 4, NotSerialized) // _DSM: Device-Specific Method
                {
          If(!Arg2) { Return(Buffer(){0x03}) }
          Return(Package(){
              "built-in", Buffer(){0x00}, "device_type", ,
              Buffer(){"Thermal-Controller"}, "model",
              Buffer(){"Sunrise Point-LP Thermal subsystem"}, "name",
              Buffer(){"Sunrise Point-LP Thermal subsystem"}, "AAPL,slot-name",
              Buffer(){"Internal@0,20,2"}, "compatible",
              Buffer(){"pci8086,9d21"},
          })
                }
            }
        }
    }


// EOF
